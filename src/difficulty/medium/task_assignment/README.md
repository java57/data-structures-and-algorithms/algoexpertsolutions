**Problem Statement**

```
A function that takes a list of tasks and number of workers.
One worker should be able to work on 2 and only 2 tasks.
We need to optimize tasks assignment such that the tasks are completed in the fastest possible time
```


**Example**
```
Input
---------------------------
workers = 3
tasks = [1, 3, 5, 3, 1, 4]


Output
---------------------------
One possible output is 
[[0, 2], [4, 5], [1, 3]]
If you see at index, 0 and 2 max time taken would be 6 and rest of the combination would be in or within this time.
That's why this is an optimal solution. 
```