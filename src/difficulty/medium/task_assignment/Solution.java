package difficulty.medium.task_assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/***
 * Basic idea to solve this problem is to get task with minimum time and task with maximum time and assign it to one worker
 * And in similar manner, assign tasks to other workers
 */
public class Solution {

  public static void main(String[] args) {
    System.out.println(taskAssignment(3, Stream.of(1, 3, 5, 3, 1, 4).collect(Collectors.toCollection(ArrayList::new))));
  }

  public static ArrayList<ArrayList<Integer>> taskAssignment(int workers, ArrayList<Integer> tasksToComplete){
    Map<Integer, ArrayList<Integer>> taskDurationIndices = getTaskDuration(tasksToComplete);
    ArrayList<ArrayList<Integer>> output = new ArrayList<>();
    Collections.sort(tasksToComplete);
    for(int i=0, j=tasksToComplete.size();i<j;i++,j--){
      ArrayList<Integer> tasksList = new ArrayList<>();
      tasksList.add(taskDurationIndices.get(tasksToComplete.get(i)).get(0));
      taskDurationIndices.get(tasksToComplete.get(i)).remove(0);
      tasksList.add(taskDurationIndices.get(tasksToComplete.get(j-1)).get(0));
      taskDurationIndices.get(tasksToComplete.get(j-1)).remove(0);
      output.add(tasksList);
    }
    return output;
  }

  public static Map<Integer, ArrayList<Integer>> getTaskDuration(ArrayList<Integer> tasks){
    Map<Integer, ArrayList<Integer>> taskDurationIndices = new HashMap<>();
    for(int i=0; i<tasks.size(); i++){
      if(taskDurationIndices.containsKey(tasks.get(i))){
        taskDurationIndices.get(tasks.get(i)).add(i);
      }
      else{
        ArrayList<Integer> indices = new ArrayList<>();
        indices.add(i);
        taskDurationIndices.put(tasks.get(i), indices);
      }
    }
    return taskDurationIndices;
  }
}
